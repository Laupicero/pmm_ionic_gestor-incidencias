----------------------------------------------------------
				INSTRUCCIONES DE PABLO

EJERCICIO: Gestión de incidencias en una red empresarial
----------------------------------------------------------

Se trata de elaborar un pequeña aplicación Angular con IONIC 
que sirva para gestionar las incidencias que sucedan en la red informática de una empresa.
El diagrama adjunto os ayudará a la realización del interfaz con IONIC.

Requisitos:
- Cuando se pulsa la primera pestaña simplemente aparece la información de la empresa en forma de página estática.
- Cuando se pulsa la segunda pestaña aparece un botón que permite añadir incidencias y un listado de las actuales.
- Cuando se pulsa una incidencia concreta de la lista se muestra una página con los detalles, donde podremos marcar la incidencia como solucionada (desapareciendo así de la lista).

Notas:
- Utiliza un servicio para almacenar los datos de las incidencias. De cada incidencia debes guardar el nombre, la descripción, la fecha en la que se produjo y su estado de resolución.
- Cuando se pulse el botón de añadir incidencias surgirá una ventana modal donde aparecerá el formulario para rellenar los datos de la nueva incidencia. Debes transferir los datos desde la ventana modal hasta la página del listado inicial con los mecanismos ofrecidos por IONIC y vistos en clase.
- Para pasar los datos a la página de detalles de la incidencia debes usar rutas parametrizadas.