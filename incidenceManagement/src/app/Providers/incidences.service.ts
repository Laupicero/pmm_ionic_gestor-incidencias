import { Injectable } from '@angular/core';
import {IncidencesDAO} from '../Model/AlmuniaIncidences';
import { Incidence } from '../Model/Incidence';

@Injectable({
  providedIn: 'root'
})
export class IncidencesService {
  private incidences: Incidence[];

  constructor() {
    this.incidences = IncidencesDAO;
   }

  // -------------------
  // MÉTODOS
  // -------------------
  getAllIncidences() {
    return [...this.incidences];
  }

    // Añadir Incidencia
    addIncidence(name: string, details: string): void {
      this.incidences.push(new Incidence(name, details));
      console.log(this.incidences)
    }
  
    // Eliminar Incidencia
    removeIncidence(inc: Incidence): void {
      let i = this.incidences.indexOf(inc);
      this.incidences.splice(i, 1);
    }

    //Encuentra Incidencia
    getIncidence(nameInc: string): Incidence{
      return {...this.incidences.find(inc => inc.name === nameInc)}
    }
}
