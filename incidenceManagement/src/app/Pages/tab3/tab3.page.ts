import { Component } from '@angular/core';
import {  FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { AlertController } from '@ionic/angular';
import { IncidencesService } from '../../Providers/incidences.service';

@Component({
  selector: 'app-tab3',
  templateUrl: 'tab3.page.html',
  styleUrls: ['tab3.page.scss']
})
export class Tab3Page {
  incidencesForm: FormGroup;

  // Constructor
  constructor(
    private incidencesService: IncidencesService,
    private formBuilder: FormBuilder,
    private alertController: AlertController) {

      this.incidencesForm = this.formBuilder.group({
        name: new FormControl('', Validators.required),
        details: new FormControl('', Validators.required)
      });
    }


  // MÉTODOS

  // Llama al Servicio para crear una nueva incidencia
  addNewIncidence(name: string, details:string){
    if(this.incidencesForm.valid){
      this.incidencesService.addIncidence(name, details);
      
    } else if (this.incidencesForm.controls.name.hasError('required') || this.incidencesForm.controls.details.hasError('required')) {
      this.alertMsg('Empty Field', 'Please fill all fields to submit a new course');

    } else {
      this.alertMsg('Wrong value', 'Score must be between 0 and 5, please enter a valid value');
    }
  }



    // Mensaje Alerta
    async alertMsg(header: string, message: string) {
      const alert = await this.alertController.create({
        header,
        message,
        buttons: ['OK']
      });
      await alert.present();
    }

}
