import { Component, OnInit } from '@angular/core';
import { IncidencesService } from 'src/app/Providers/incidences.service';
import { Incidence } from '../../Model/Incidence';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-tab2',
  templateUrl: 'tab2.page.html',
  styleUrls: ['tab2.page.scss']
})
export class Tab2Page implements OnInit{
  incidences: Incidence[];

  // Inyectamos el servicio
  constructor(
    private incidencesService: IncidencesService,
    private router: Router
  ) {}

  //OnInit
  ngOnInit(): void {
    this.incidences = this.incidencesService.getAllIncidences();
  }

  ionViewWillEnter(){
    this.incidences = this.incidencesService.getAllIncidences();
  }

  deleteIncidence(inc: Incidence): void{
    this.incidencesService.removeIncidence(inc);
    this.incidences = this.incidencesService.getAllIncidences();
  }

  navigateToDetailsPage(nameIncidence: string):void{
    this.router.navigate(['incidence-details', nameIncidence]);
  }

}
