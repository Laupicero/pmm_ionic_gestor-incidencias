import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { IncidenceDetailsPageRoutingModule } from './incidence-details-routing.module';

import { IncidenceDetailsPage } from './incidence-details.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    IncidenceDetailsPageRoutingModule
  ],
  declarations: [IncidenceDetailsPage]
})
export class IncidenceDetailsPageModule {}
