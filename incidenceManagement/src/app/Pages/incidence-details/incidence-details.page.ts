import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { NavController } from '@ionic/angular';
import { Incidence } from '../../Model/Incidence';
import { IncidencesService } from '../../Providers/incidences.service';

@Component({
  selector: 'app-incidence-details',
  templateUrl: './incidence-details.page.html',
  styleUrls: ['./incidence-details.page.scss'],
})
export class IncidenceDetailsPage implements OnInit {
  incident: Incidence;

  constructor(
    private navCtrl: NavController,
    private route: ActivatedRoute,
    private incidencesService: IncidencesService
  ) {}

  ngOnInit() {
    this.route.paramMap.subscribe(paramMap => {
      if (!paramMap.has('name')) {
        this.navCtrl.navigateBack('/tabs/tab2');
        return;
      }
      this.incident = this.incidencesService.getIncidence(paramMap.get('name'));
    });
  }

}
