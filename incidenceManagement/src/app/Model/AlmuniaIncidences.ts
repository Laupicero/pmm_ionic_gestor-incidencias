import { Incidence } from './Incidence';

export const IncidencesDAO: Incidence[] = [
    new Incidence(`Persiana Rota`, `Se ha roto la persiana de la clase 3º E.S.O. A`),
    new Incidence(`Proyector Estropeado`, `Proyector de la Clase 2ºDAM deja de funcionar a ratos`),
    new Incidence(`Móvil Perdido`, `Alumno de 2º E.S.OB ha perdido el móvil durante la clase de Educación Física`),
    new Incidence(`Puerta Estropeada`, `Se ha roto el pomo de la puerta del aula de tecnología`)
];